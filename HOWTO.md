nakeDeb
=======

[nakeDeb homepage](https://nakedeb.arpinux.org)

* dépendances pour les paquets debian : equivs
* dépendances pour live-build : live-build live-tools
* dépendance pour construire le fichier torrent : mktorrent

construire une ISO :
--------------------

* éditer le script 'nakedbuild' pour ajuster les variables
* éditer le contenu de 'auto/' et 'config/' selon vos besoins
* vérifier les mises à jour de veracrypt
* lancer **en mode administrateur** :
  * `./nakedbuild 32`      > nakedeb i386 (~837M)
  * `./nakedbuild 64`      > nakedeb amd64 (~768M)
  * `./nakedbuild 32 nf`   > nakedeb i386 non-free (~1.01G)
  * `./nakedbuild 64 nf`   > nakedeb amd64 non-free (~970M)
  * `./nakedbuild 32 full` > nakedeb i386 non-free full system (non distribuée)
  * `./nakedbuild 64 full` > nakedeb amd64 non-free full system (non distribuée)
  * `./nakedbuild clean`   > nettoyage du dossier de construction et du cache

assurez-vous de lancer `nakedbuild clean` avant de construire une autre architecture

------

option : apt-cacher-ng

* installer : `apt install apt-cacher-ng`
* configurer : `export http_proxy=http://localhost:3142/`
* activer : `systemctl start apt-cacher-ng`
* utiliser : déjà intégré dans le script de build

------

comment convertir les pages d'aide html en manpage

* 1- passer du html au markdown avec 
`pandoc page.html -t markdown -o page.md`
* 2- éditer pour supprimer les liens vers les images et la navigation
* 3- passer du markdown au man avec 
`pandoc page.md -s -t man -o page.7`
* fix header & footer pour la catégorie du man (ici 7)

exemple :

```
.TH NAKEDCLI 7 "date" "texte en bas" "titre du haut"
.SH NAME
nakedcli \- manuel des commandes de base Debian et des commandes *nakeDeb
.hy
.SS Mémo des commandes de base
```

```
.VOIR AUSSI
nakedeb(7), nakedkbd(7)
.SH AUTHORS
arnault perret <nakedev@arpinux.org>.
```

------

option : signer le fichier md5 pour distribuer l'ISO

`gpg --detach-sig --sign nakedeb-${VERSION}.md5`

en remplaçant ${VERSION} par la version de votre nakedeb.

------

aide, contact, retours :

* forum : https://nakedeb.arpinux.org/crew
* IRC : #nakedeb on oftc.net
* contact : nakedeb@arpinux.org
