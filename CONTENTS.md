nakeDeb sources content
=======================

les sources nakeDeb contiennent de quoi construire 
la distribution livrée sous forme d'ISO  
les paquets additionnels sont issus du dépôt git 
https://framagit.org/3hg/nakedpkgs

[auto](/auto)
-------------
- [auto/build](/auto/build) : live-build build script > chope la version dans 
le ./nakedbuild pour y ajouter les logs de build
- [auto/clean](/auto/clean) : live-build clean script > nettoyer le dossier 
de build après contruction
- [auto/config](/auto/config) : live-build config script > définir certaines 
options de lb config pour toutes les versions

[config](/config)
-----------------
- [config/archives](/config/archives/) : dépôt et clé gpg nakeDeb
- [config/hooks](/config/hooks/) : scripts lancés lors du build dans le chroot, 
juste avant la compression en squashfs
- [config/includes.binary](config/includes.chroot/) : fichiers de configuration 
pour isolinux et grub, les lanceurs pour le liveCD
- [config/package-lists](config/package-lists/) : les listes des paquets nécessaires.
  - nakedeb.list.chroot pour la version "main"
  - nakedeb-nf.list.chroot pour la version "non-free"
  - nakedeb-full.list.chroot pour la version "full"
  - nakedeb.list.binary pour le live : (les paquets UEFI pour une installation 
  hors-ligne)

[ressources](/ressources)
-------------------------
certaines ressources sont récupérées directement lors du build
- [ressources/img](/ressources/img/) : sources pour le logo et les éléments 
graphiques nakeDeb'. certains éléments sont récupérés pour le site web
- [ressources/veracrypt](/ressources/veracrypt/) : les archives à installer 
pour chaque architecture (récupérées lors du build)
- [ressources/mkp2p](/ressources/mkp2p) : le script de 
génération des torrents pour nakeDeb (récupéré lors du build)
- [ressources/nakedpreseed](/ressources/nakedpreseed) : le fichier de 
configuration preseed pour nakeDeb (récupéré lors du build)
- [ressources/nonfree.list](/ressources/nonfree.list) : le fichier sources.list 
pour les version non-free et full (récupéré lors du build)
- [ressources/trackers.txt](ressources/trackers.txt) : liste des trackers 
publics à coller lors de la création des .torrents

[nakedbuild](/nakedbuild)
-------------------------
script principal de construction de l'ISO. commenté.

[howto](/HOWTO.md) & [help](/HELP)
----------------------------------
mode d'emploi complet & raccourci

