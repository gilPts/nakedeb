#!/bin/bash
# nakedeb build script
# nak3dev @ 2020-2022 © WTFPL <https://arpinux.org>

# cfg ------------------------------------------------------------------
PROJECT="nakedeb project"
HOMEPAGE="https://nakedeb.arpinux.org"
CONTACT="nakedeb@arpinux.org"
VERSION="nakedeb-1.4.3"
DEBV="deb$(cat /etc/debian_version)"
DISTRO="bullseye"
NKDIR="$(realpath "$(dirname "$0")")"
LOG="${NKDIR}/${VERSION}.log"

# txt & vars -----------------------------------------------------------
NFLIST="${NKDIR}/config/package-lists/nakedeb-nf.list.chroot"
FULLLIST="${NKDIR}/config/package-lists/nakedeb-full.list.chroot"
RESET="## automatic edition by nakedbuild\n## DO NOT EDIT!"
#liste des firmwares de la version nonfree
NFPKGS="vrms firmware-linux firmware-linux-nonfree firmware-misc-nonfree \
intel-microcode firmware-realtek amd64-microcode firmware-amd-graphics \
bluez-firmware firmware-atheros firmware-bnx2 firmware-bnx2x \
firmware-brcm80211 firmware-b43-installer firmware-b43legacy-installer \
firmware-ipw2x00 firmware-iwlwifi"
#liste des paquets de la version full (ajoutés à la liste nonfree)
FULLPKGS="firefox-esr thunderbird newsboat filezilla nmap \
gthumb gimp inkscape mcomix audacity handbrake pitivi easytag \
libreoffice libreoffice-gtk3 libreoffice-l10n-fr font-manager \
ttf-aenigma fonts-liberation fonts-tibetan-machine fonts-ipaexfont \
info4help tkmenu unison-gtk git build-essential make neovim armagetronad \
debian-reference-fr lescahiersdudebutant thebeginnershandbook \
broadcom-sta-dkms atmel-firmware dahdi-firmware-nonfree firmware-ath9k-htc \
firmware-cavium firmware-intel-sound firmware-intelwimax firmware-ivtv \
firmware-libertas firmware-myricom firmware-netronome firmware-netxen \
firmware-qcom-media firmware-qlogic firmware-samsung firmware-siano \
firmware-ti-connectivity firmware-zd1211 hdmi2usb-fx2-firmware"

# stop on error and try to trace it ------------------------------------
set -eE # same as: `set -o errexit -o errtrace`

# cd work directory ----------------------------------------------------
cd ${NKDIR}

# root -----------------------------------------------------------------
if [ $(whoami) != root ]; then
    echo -e "\nERROR: nakedbuild needs root\n"; exit 1
fi

# args & help ----------------------------------------------------------
if [ "$1" == "32" ]; then ARCH="i386"
elif [ "$1" == "64" ]; then ARCH="amd64"
elif [ "$1" == "clean" ]; then
    lb clean
    rm -Rf cache
    echo -e "${RESET}" > ${VCHOOK}
    echo -e "${RESET}" > ${NFLIST}
    echo -e "${RESET}" > ${FULLLIST}
    exit 0
else
    # no args or wrong args > help
    echo -e "\n wrong arg... need help ?\n"; cat ${NKDIR}/HELP; exit 1
fi

# open log -------------------------------------------------------------
# ouverture du fichier de log avec les informations sur le système hôte
echo -e "----\n${PROJECT} - ${HOMEPAGE} - ${CONTACT}" > ${LOG}
echo -e "$(date)" >> ${LOG}
echo -e "system: $(uname -a)" >> ${LOG}
echo -e "tools: live-build-$(lb -v) - Debian-$(cat /etc/debian_version)" >> ${LOG}
if [ "$2" == "nf" ]; then
    echo -e "> building ${VERSION} ${ARCH} ${DISTRO} non-free" >> ${LOG}
elif [ "$2" == "full" ]; then
    echo -e "> building ${VERSION} ${ARCH} ${DISTRO} non-free full" >> ${LOG}
else
    echo -e "> building ${VERSION} ${ARCH} ${DISTRO} main" >> ${LOG}
fi

# setup non-free/full options ------------------------------------------
if [ "$2" == "nf" ] || [ "$2" == "full" ]; then
    echo -e "----\nINFO: setting up non-free sources.list" >> ${LOG}
    mkdir -p config/includes.chroot/etc/apt/sources.list.d
    cp ressources/nonfree.list config/includes.chroot/etc/apt/sources.list.d/
fi
# setup preseed --------------------------------------------------------
# mise en place du fichier preseed pour la fin de l'installation.
echo -e "----\nINFO: setting up naked preseed" >> ${LOG}
mkdir -p config/includes.chroot/usr/bin
install -m 755 ressources/nakedpreseed config/includes.chroot/usr/bin/nakedpreseed

# configure build ------------------------------------------------------
echo -e "----\nINFO: configuration de ${VERSION}-${ARCH}" >> ${LOG}
# main/nonfree/full versions
if [ "$2" == "nf" ]; then
    echo -e "----\nINFO: setting up non-free release" >> ${LOG}
    ARCHIVE_AREAS="main contrib non-free"
    L_PACKAGE="linux-image"
    echo -e "${NFPKGS}" > ${NFLIST}
    echo -e "${RESET}" > ${FULLLIST}
elif [ "$2" == "full" ]; then
    echo -e "----\nINFO: setting up non-free full system release" >> ${LOG}
    ARCHIVE_AREAS="main contrib non-free"
    L_PACKAGE="linux-image linux-headers"
    echo -e "${NFPKGS}" > ${NFLIST}
    echo -e "${FULLPKGS}" > ${FULLLIST}
else
    echo -e "----\nINFO: setting up main system release" >> ${LOG}
    ARCHIVE_AREAS="main contrib"
    L_PACKAGE="linux-image"
    echo -e "${RESET}" > ${NFLIST}
    echo -e "${RESET}" > ${FULLLIST}
fi
# common config
lb config -a "${ARCH}" \
    --iso-volume "${VERSION}" \
    --iso-application "${VERSION}" \
    --iso-publisher "${PROJECT}; ${HOMEPAGE}; ${CONTACT}" \
    --parent-distribution "${DISTRO}" \
    --distribution "${DISTRO}" \
    --parent-archive-areas "${ARCHIVE_AREAS}" \
    --archive-areas "${ARCHIVE_AREAS}" \
    --linux-package "${L_PACKAGE}" | tee -a ${LOG}
# ordinosaures
if [ "${ARCH}" == "i386" ]; then
    lb config --linux-flavours "686" | tee -a ${LOG}
fi
# if apt-cacher-ng
if dpkg-query -W apt-cacher-ng &>"/dev/null"; then
    export http_proxy="http://127.0.0.1:3142/"
    lb config --apt-http-proxy "${http_proxy}"
fi

# build ISO ------------------------------------------------------------
echo -e "----\nINFO: construction de ${VERSION}-${ARCH}-${DISTRO}\n" >> ${LOG}
lb build

# rename ---------------------------------------------------------------
if test -f live-image-${ARCH}.hybrid.iso; then
    echo -e "----\nINFO: renommer"
    ISODIR="${NKDIR}/nakedeb-${ARCH}"
    if [ "$2" == "nf" ]; then
        NAME="${VERSION}-${DEBV}-${ARCH}-nonfree"
    elif [ "$2" == "full" ]; then
        NAME="${VERSION}-${DEBV}-${ARCH}-nonfree-full"
    else
        NAME="${VERSION}-${DEBV}-${ARCH}"
    fi
    mkdir -p ${ISODIR}
    mv live-image-${ARCH}.hybrid.iso ${ISODIR}/${NAME}.iso
    mv chroot.packages.install ${ISODIR}/${NAME}.pkgs
    mv ${LOG} ${ISODIR}/${NAME}.log
    cd ${ISODIR}
    echo -e "----\nINFO: calculate md5sum"
    md5sum ${NAME}.iso > ${NAME}.md5
    echo -e "----\nINFO: make torrent"
    cp ${NKDIR}/ressources/mkp2p ${ISODIR}
    ./mkp2p ${NAME}.iso
    rm mkp2p
    cd ${NKDIR}
    echo -e "----\nINFO: permissions"
    chown -R ${SUDO_USER}:${SUDO_USER} ${ISODIR}
    echo -e "----\nINFO: nettoyage"
    echo -e "${RESET}" > ${NFLIST}
    echo -e "${RESET}" > ${FULLLIST}
    lb clean
    echo -e "----\nbuild made in $(($SECONDS/60)) minutes\n"
    echo -e "MEMO: you should gpgsign md5 files\n"; exit 0
else
    chmod 666 ${LOG}
    echo -e "----\nERROR: ISO not builded ... check your ${LOG} file !\n"; exit 1
fi
# eof ------------------------------------------------------------------
